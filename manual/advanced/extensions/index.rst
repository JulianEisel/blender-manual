.. _extensions-index:
.. index:: Extensions

*******************
Creating Extensions
*******************

Extensions are **add-ons** or **themes** used to extend the core functionality of Blender.
They are shared in online platforms, and can be installed and updated from within Blender.

The official extensions platform for the Blender project is `extensions.blender.org <https://extensions.blender.org>`__.
Other third party sites can also be supported, as long as they follow the Extensions Platform specification.

.. seealso::

   For the extension settings, and how to manage them, refer to the
   :doc:`User Preferences </editors/preferences/extensions>`.

.. toctree::
   :maxdepth: 1

   Getting started <getting_started.rst>
   Compatible licenses <licenses.rst>
   Supported tags <tags.rst>
   Add-ons <addons.rst>
   Python Wheels <python_wheels.rst>
   Command Line Arguments <command_line_arguments.rst>
   Creating a Repository <creating_repository/index.rst>
