.. _particles-hair-index:

########
  Hair
########

This page is about the end of life hair system.
Read about the new hair system on the :doc:`Hair Nodes page </modeling/geometry_nodes/hair/index>`.

.. toctree::
   :maxdepth: 2

   introduction.rst
   emission.rst
   dynamics.rst
   render.rst
   shape.rst
   children
   display.rst
