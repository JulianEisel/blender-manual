
###################
  Transform Nodes
###################

These nodes distort the image in some fashion, operating either uniformly on the image,
or by using a mask to vary the effect over the image.

.. toctree::
   :maxdepth: 1

   rotate.rst
   scale.rst
   transform.rst
   translate.rst

----------

.. toctree::
   :maxdepth: 1

   corner_pin.rst
   crop.rst

----------

.. toctree::
   :maxdepth: 1

   displace.rst
   flip.rst
   map_uv.rst

----------

.. toctree::
   :maxdepth: 1

   lens_distortion.rst
   movie_distortion.rst
