
#############
  Selecting
#############

.. toctree::
   :maxdepth: 2

   introduction.rst
   mirror.rst
   random.rst
   checker_deselect.rst
   more_less.rst
   similar.rst
   all_by_trait.rst
   linked.rst
   loops.rst
   sharp_edges.rst
   side_of_active.rst
   by_attribute.rst
