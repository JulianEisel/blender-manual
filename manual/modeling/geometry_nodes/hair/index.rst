.. index:: Geometry Nodes; Hair

##############
  Hair Nodes
##############

Nodes focussed on generating or editing curves, typically used for hair.

.. toctree::
   :maxdepth: 2

   Deformation <deformation/index.rst>
   Generation <generation/index.rst>
   Guides <guides/index.rst>
   Read <read/index.rst>
   Utility <utility/index.rst>
   Write <write/index.rst>
