
*********
Modifiers
*********

Operators for working with an object's :doc:`/modeling/modifiers/index`.


.. _bpy.ops.object.gpencil_modifier_add:
.. _bpy.ops.object.modifier_add:

Add Modifier
============

.. reference::

   :Mode:      Object Mode
   :Menu:      :menuselection:`Object --> Modifiers --> Add Modifier`

Opens a menu with a list of modifiers, selecting a modifier will add it to the bottom of :ref:`modifier-stack`.


.. _bpy.ops.object.modifiers_copy_to_selected:

Copy Modifiers to Selected Objects
==================================

.. reference::

   :Mode:      Object Mode
   :Menu:      :menuselection:`Object --> Modifiers --> Copy Modifiers to Selected Objects`

Copies all modifiers from the :term:`Active` object to the other selected objects.


.. _bpy.ops.object.modifiers_clear:

Clear Object Modifiers
======================

.. reference::

   :Mode:      Object Mode
   :Menu:      :menuselection:`Object --> Modifiers --> Clear Object Modifiers`

Deletes all modifiers from the selected objects.
