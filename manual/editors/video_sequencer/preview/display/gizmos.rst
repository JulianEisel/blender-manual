.. |gizmo-icon| image:: /images/editors_3dview_display_gizmo_header.png
.. _bpy.types.SpaceSequenceEditor.show_gizmo:

******
Gizmos
******

.. reference::

   :Header:    |gizmo-icon| :menuselection:`Gizmos`

Clicking the icon toggles all gizmos in the Video Sequencer.
The drop-down button displays a popover with more detailed settings,
which are described below.

.. _bpy.types.SpaceSequenceEditor.show_gizmo_navigate:

Navigate
   Enable/disable the navigation gizmo.

.. _bpy.types.SpaceSequenceEditor.show_gizmo_tool:

Active Tools
   Enable/disable the gizmo of the active tool.
