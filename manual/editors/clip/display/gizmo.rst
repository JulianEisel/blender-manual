.. |gizmo-icon| image:: /images/editors_3dview_display_gizmo_header.png
.. _bpy.types.SpaceClipEditor.show_gizmo:

***************
Viewport Gizmos
***************

.. reference::

   :Mode:      All Modes
   :Header:    |gizmo-icon| :menuselection:`Gizmos`

Clicking the icon toggles all the gizmos in the Movie Clip Editor.
The drop-down button displays a popover with more detailed settings,
which are described below.


Viewport Gizmos
===============

.. _bpy.types.SpaceClipEditor.show_gizmo_navigate:

Navigate
   Toggle the visibility of the zooming and panning gizmos in the upper right corner.
